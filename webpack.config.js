const merge = require('webpack-merge');
const HappyPack = require('happypack');
const webpack = require('webpack');
const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const HtmlWebpackExcludeAssetsPlugin = require('html-webpack-exclude-assets-plugin');
const parts = require('./webpack.parts');
const routes = require('./routes');

const PATHS = {
  app: path.join(__dirname, 'src'),
  dist: path.join(__dirname, 'dist'),
  build: path.join(__dirname, 'build'),
};

const commonConfig = merge([
  {
    entry: {
      main: ['@babel/polyfill', path.join(PATHS.app, 'js')],
    },
    resolve: {
      alias: {
        images: path.join(PATHS.app, 'assets', 'images'),
        fonts: path.join(PATHS.app, 'assets', 'fonts'),
        uploads: path.join(PATHS.app, 'assets', 'uploads'),
        favicons: path.join(PATHS.app, 'assets', 'favicons'),
      },
    },
    plugins: [
      new HappyPack({
        loaders: [
          // Capture Babel loader
          'babel-loader',
        ],
      }),
      new webpack.ContextReplacementPlugin(/moment[/\\]locale$/, /ru|uk|en/),
      // new BundleAnalyzerPlugin(),
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
      }),
      new CopyPlugin([
        {
          from: './src/assets/favicons',
          to: './favicons',
        },
      ]),
    ],
  },
  // parts.loadEJS({}),
  parts.loadJavaScript({ include: PATHS.app }),
]);

const productionConfig = merge([
  {
    output: {
      chunkFilename: '[name].[chunkhash:4].js',
      filename: '[name].[chunkhash:4].js',
      publicPath: '/',
      path: PATHS.build,
    },
    stats: {
      // One of the two if I remember right
      // entrypoints: false,
      children: false,
    },
  },
  parts.clean(['build']),
  parts.extractCSS({
    use: [
      'css-loader?sourceMap',
      parts.autoprefix(),
      parts.discardDuplicates(),
      'resolve-url-loader?sourceMap',
      'sass-loader?sourceMap',
    ],
  }),
  parts.loadFonts({
    options: {
      name: '[name].[ext]',
      outputPath: 'fonts/',
    },
  }),
  // parts.purifyCSS({
  //   paths: glob.sync(`${PATHS.app}/**/*.js`, { nodir: true })
  // }),
  parts.loadImages({
    options: {
      limit: 15000,
      name: '[name].[hash:4].[ext]',
      outputPath: 'images/',
    },
  }),
  parts.generateSourceMaps({ type: 'cheap-module-source-map' }),
  {
    optimization: {
      splitChunks: {
        cacheGroups: {
          commons: {
            test: /[\\/]node_modules[\\/]/,
            name: 'vendors',
            chunks: 'initial',
          },
        },
      },
      runtimeChunk: {
        name: 'manifest',
      },
    },
  },
  {
    recordsPath: path.join(__dirname, 'records.json'),
  },
  {
    performance: {
      hints: 'warning',
      maxEntrypointSize: 50000,
      maxAssetSize: 450000,
    },
  },
  //   parts.minifyJavaScriptUglify(),
  parts.minifyJavaScriptTerser(),
  parts.minifyCSS({
    options: {
      discardComments: {
        removeAll: true,
      },
      // Run cssnano in safe mode to avoid
      // potentially unsafe transformations.
      safe: true,
    },
  }),
]);

const developmentConfig = merge([
  {
    output: {
      publicPath: '/',
      path: PATHS.dist,
    },
  },
  parts.clean(['dist']),
  parts.devServer({
    host: process.env.HOST,
    port: process.env.PORT,
  }),
  parts.loadSASS({
    use: [
      'style-loader?sourceMap',
      'css-loader?sourceMap',
      parts.autoprefix(),
      // parts.discardDuplicates(),
      'resolve-url-loader?sourceMap',
      'sass-loader?sourceMap',
    ],
  }),
  parts.loadImages(),
  parts.loadFonts(),
]);

module.exports = (mode) => {
  process.env.BABEL_ENV = mode;

  const pages = routes.map((route) => ({
    title: route.title,
    path: route.path,
    template: path.join(PATHS.app, 'html', 'views', route.template),
    chunks: ['main', 'manifest', 'vendors'],
  }));

  const config = mode === 'production' ? productionConfig : developmentConfig;

  const siteMap =
    mode === 'production'
      ? []
      : [
          {
            title: 'Site Map',
            template: path.join(PATHS.app, 'js', 'views', '@.js'),
            path: '@',
            siteMap: pages.map(({ title, path }) => ({ title, path })),
            chunks: [],
          },
        ];

  return merge(
    [commonConfig, config, { mode }].concat(
      [...pages, ...siteMap].map((params) => parts.page(params)),
      { plugins: [new HtmlWebpackExcludeAssetsPlugin()] }
    )
  );
};
