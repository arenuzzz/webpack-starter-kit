module.exports = function(templateParams) {
  const getListItem = ({ path, title }) =>
    `<li style="font-size: 20px"><a href="${path || '/'}">${title}</a></li>`;

  const getList = (data) => {
    return `<ul>
        ${data.map((itemData) => getListItem(itemData)).join('')}
    </ul>`;
  };

  const { siteMap } = templateParams.htmlWebpackPlugin.options;

  const html = `
  <html>
    <head>
      <title>
      ${templateParams.htmlWebpackPlugin.options.title}
      </title>
    </head>
    <body>
    ${siteMap && Array.isArray(siteMap) ? getList(siteMap) : ''}
    </body>
  </html>`;

  return html;
};
